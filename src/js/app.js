"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import tabs from './components/tabs.js'; // Tabs
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import Spoilers from "./components/spoilers.js";
import Dropdown from "./components/dropdown.js";
import priorityNav from 'priority-nav';
import {WebpMachine} from "webp-hero"

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();

// Вкладки (tabs)
tabs();

// Сворачиваемые блоки
collapse();

// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

// Dropdown
Dropdown();


// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false
});

// Sliders
import "./components/sliders.js";

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-add-favorite]')) {
        event.target.closest('[data-add-favorite]').classList.toggle('added');
    }
})

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-partners-view]')) {
        event.target.closest('[data-partners]').querySelector('[data-partners-list]').classList.toggle('view');
    }
})


window.addEventListener("load", function (e) {

// Header Collapse Navigation
    const navLabel = document.querySelector('[data-nav-label]').dataset.navLabel
    let nav = priorityNav.init({
        mainNavWrapper: '.header-nav', // mainnav wrapper selector (must be direct parent from mainNav)
        mainNav: '.header-nav__content', // mainnav selector. (must be inline-block)
        navDropdownLabel: navLabel,
        navDropdownClassName: 'header-nav__dropdown', // class used for the dropdown.
        navDropdownToggleClassName: 'header-nav__toggle', // class used for the dropdown toggle.
    });

});


