/*
Документация по работе в шаблоне:
Документация слайдера: https://swiperjs.com/
Сниппет(HTML): swiper
*/

import Swiper, {Autoplay, Navigation, Pagination, Thumbs} from 'swiper';

// Инициализация слайдеров
function initSliders() {
    // Перечень слайдеров
    if (document.querySelector('[data-swiper]')) {
        new Swiper('[data-swiper]', {
            // Подключаем модули слайдера
            // для конкретного случая
            //modules: [Navigation, Pagination],
            /*
            effect: 'fade',
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            */
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 0,
            autoHeight: true,
            speed: 800,
            //touchRatio: 0,
            //simulateTouch: false,
            //loop: true,
            //preloadImages: false,
            //lazy: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            // Arrows
            navigation: {
                nextEl: '.swiper__more .swiper__more--next',
                prevEl: '.swiper__more .swiper__more--prev',
            },
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 16,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 24,
                },
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 32,
                },
            },
            on: {}
        });
    }

    if (document.querySelector('[data-programs]')) {
        new Swiper('[data-programs]', {
            modules: [Navigation],
            observer: true,
            observeParents: true,
            slidesPerView: "auto",
            spaceBetween: 0,
            navigation: {
                nextEl: '[data-programs-next]',
                prevEl: '[data-programs-prev]',
            },
            breakpoints: {
                992: {
                    slidesPerView: 2,
                },
                1200: {
                    slidesPerView: 3,
                },
            },
        })
    }

    if (document.querySelector('[data-headliners]')) {
        new Swiper('[data-headliners]', {
            modules: [Navigation],
            observer: true,
            observeParents: true,
            slidesPerView: "auto",
            spaceBetween: 0,
            navigation: {
                nextEl: '[data-headliners-next]',
                prevEl: '[data-headliners-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 3,
                },
                1200: {
                    slidesPerView: 4,
                },
            },
        })
    }

    if (document.querySelector('[data-tabs-menu]')) {
        new Swiper('[data-tabs-menu]', {
            observer: true,
            observeParents: true,
            slidesPerView: "auto",
            spaceBetween: 25,
        })
    }
}


window.addEventListener("load", function (e) {
    // Запуск инициализации слайдеров
    initSliders();
});
